import { Scale, type Types } from 'phaser'

const conf: Types.Core.GameConfig = {
  mode: Scale.RESIZE,
  height: 420,
  width: 420,
  pixelArt: true,
  physics: {
    default: "arcade",
    arcade: {
      debug: false
    }
  }
}

export default conf
