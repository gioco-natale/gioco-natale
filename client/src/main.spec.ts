import { Game } from 'phaser'
import main from './main'

describe('main', () => {
  describe('make game', () => {
    it('should start a Game instance', async () => {
      expect((await main) instanceof Game).toBe(true)
    })
  })
})
