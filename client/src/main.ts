import * as phaser from 'phaser'
import conf from './conf'
import { BootScene } from './scenes/boot.scene'

export default (async function main () {
  const game = new phaser.Game(conf)
  game.scene.add('boot', new BootScene())
  game.scene.start('boot')
  return game
}())
