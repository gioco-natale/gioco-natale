import { Scene } from 'phaser'
import { PlayScene } from './play.scene'
import TileMapScene from './tilemap/tilemap.scene'
import Demo from './demo.scene'

export class BootScene extends Scene {
  constructor (conf = {}) {
    super({
      ...conf,
      key: 'boot'
    })
  }

  init (): void {
    if (!this.scene.get('play')) {
      this.scene.add('play', new PlayScene())
    }
    if (!this.scene.get('tilemap')) {
      this.scene.add('tilemap', new TileMapScene())
    }
    if (!this.scene.get('demo')) {
      this.scene.add('demo', new Demo())
    }
  }

  preload (): void {
    this.load.image('background', 'assets/character and tileset/demonstration.png')
    this.load.spritesheet("priest", "assets/character and tileset/Dungeon_Character_2.png", {
      frameWidth: 16,
      frameHeight: 16,
      startFrame: 4,
      endFrame: 6
    })
    this.load.spritesheet("skeleton", "assets/character and tileset/Dungeon_Character_2.png", {
      frameWidth: 16,
      frameHeight: 16,
      startFrame: 11,
      endFrame: 13,
    })
    this.load.spritesheet("vampire", "assets/character and tileset/Dungeon_Character_2.png", {
      frameWidth: 16,
      frameHeight: 16,
      startFrame: 9,
      endFrame: 10,
    })
    this.load.spritesheet("ghost", "assets/character and tileset/Dungeon_Character_2.png", {
      frameWidth: 16,
      frameHeight: 16,
      startFrame: 7,
      endFrame: 8,
    })

    this.load.image('coin_1', 'assets/items and trap_animation/coin/coin_1.png')
    this.load.image('coin_2', 'assets/items and trap_animation/coin/coin_2.png')
    this.load.image('coin_3', 'assets/items and trap_animation/coin/coin_3.png')
    this.load.image('coin_4', 'assets/items and trap_animation/coin/coin_4.png')

    this.load.image('box_2_1', 'assets/items and trap_animation/box_2/box_2_1.png')
    this.load.image('box_2_2', 'assets/items and trap_animation/box_2/box_2_2.png')
    this.load.image('box_2_3', 'assets/items and trap_animation/box_2/box_2_3.png')
    this.load.image('box_2_4', 'assets/items and trap_animation/box_2/box_2_4.png')

    this.load.image('priest2_1', 'assets/Character_animation/priests_idle/priest2/v1/priest2_v1_1.png')
    this.load.image('priest2_2', 'assets/Character_animation/priests_idle/priest2/v1/priest2_v1_2.png')
    this.load.image('priest2_3', 'assets/Character_animation/priests_idle/priest2/v1/priest2_v1_3.png')
    this.load.image('priest2_4', 'assets/Character_animation/priests_idle/priest2/v1/priest2_v1_4.png')
    this.load.image('torch_1', 'assets/items and trap_animation/torch/torch_1.png')
    this.load.image('torch_2', 'assets/items and trap_animation/torch/torch_2.png')
    this.load.image('torch_3', 'assets/items and trap_animation/torch/torch_3.png')
    this.load.image('torch_4', 'assets/items and trap_animation/torch/torch_4.png')

    this.load.audio("beam_audio", ["assets/audio/light-switch-81967.mp3"])
    this.load.audio("explosion_audio", ["assets/audio/ping-82822.mp3"])
    this.load.audio("pickup_audio", ["assets/audio/sci-fi-glitch-sound-105730.mp3"])
    this.load.audio("music", ["assets/audio/dark-computer-talk-59011.mp3"])
  }

  create (): void {
    this.add.text(20, 20, 'Loading...')
    this.loadFont()
    this.events.on("font-loaded", () => {
      // play tilemap demo
      this.scene.start('play')
    })
    this.anims.create({
      key: "vampire_anim",
      frames: this.anims.generateFrameNames("vampire"),
      frameRate: 2,
      repeat: -1,
    })
    this.anims.create({
      key: "skeleton_anim",
      frames: this.anims.generateFrameNames("skeleton"),
      frameRate: 2,
      repeat: -1,
      yoyo: true
    })
    this.anims.create({
      key: "priest_anim",
      frames: this.anims.generateFrameNames("priest"),
      frameRate: 2,
      repeat: -1,
      yoyo: true
    })
    this.anims.create({
      key: "ghost_anim",
      frames: this.anims.generateFrameNames("ghost"),
      frameRate: 4,
      repeat: 1,
      hideOnComplete: true
    })

    this.anims.create({
      key: "coin_anim",
      frames: [
        { key: "coin_1" },
        { key: "coin_2" },
        { key: "coin_3" },
        { key: "coin_4" }
      ],
      frameRate: 4,
      repeat: -1,
    })

    this.anims.create({
      key: "box_anim",
      frames: [
        { key: "box_2_1" },
        { key: "box_2_2" },
        { key: "box_2_3" },
        { key: "box_2_4" }
      ],
      frameRate: 4,
      repeat: -1
    })

    this.anims.create({
      key: "player_move",
      frames: [
        { key: "priest2_1" },
        { key: "priest2_2" },
        { key: "priest2_3" },
        { key: "priest2_4" }
      ],
      frameRate: 4,
      repeat: -1
    })

    this.anims.create({
      key: "torch_anim",
      frames: [
        { key: "torch_1" },
        { key: "torch_2" },
        { key: "torch_3" },
        { key: "torch_4" }
      ],
      frameRate: 4,
      repeat: -1
    })
  }

  async loadFont() {
    document.fonts.add(await new FontFace("Crang", `url(assets/fonts/Crang.ttf)`).load());
    document.fonts.add(await new FontFace("alagard", `url(assets/fonts/alagard.ttf)`).load());
    this.events.emit(`font-loaded`)
  }
}
