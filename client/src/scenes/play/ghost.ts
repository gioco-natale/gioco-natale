import { GameObjects } from "phaser"
import { PlayScene } from "../play.scene"


export class Ghost extends GameObjects.Sprite {
    constructor(scene: PlayScene, x:number, y:number) {
        super(scene, x, y, 'ghost')
        this.play("ghost_anim")
    }
}