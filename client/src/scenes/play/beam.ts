import { GameObjects } from "phaser"
import { PlayScene } from "../play.scene"


export class Beam extends GameObjects.Sprite {
    constructor(public scene:PlayScene, x = scene.player.x, y = scene.player.y, name = "beam") {
        super(scene, x, y, name)
        this.play("torch_anim")
        scene.physics.world.enableBody(this)
        if(this.body) {
            this.body.velocity.y = -250
        }
    }

    update() {
        if(this.y < this.height) {
            this.destroy()
        }
    }
}