import { Cameras, GameObjects, Input, Scene } from 'phaser'
import conf from '../conf'

export default class MapScene extends Scene {
  background: GameObjects.TileSprite

  constructor () {
    super('map')
  }

  create (): void {
    const scale = 420 / 256 * 2
    const width = parseInt(`${conf.width ?? 0}`)
    const height = parseInt(`${conf.height ?? 0}`)

    this.background = this.add.tileSprite(0, 0, width, height, 'background')
      .setOrigin(0, 0)
      .setScale(scale / 2)
      .setAlpha(0.5)
  }

  update (time: number, delta: number): void {
    this.background.tilePositionY -= delta / 20
    this.background.tilePositionX -= delta / 40
  }
}
