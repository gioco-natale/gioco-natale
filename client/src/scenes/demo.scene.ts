import { Cameras, Input, Scene } from 'phaser'
import conf from '../conf'

export default class Demo extends Scene {
  #iter = this.getCameraPosition()
  #controls: Cameras.Controls.SmoothedKeyControl

  constructor () {
    super('demo')
  }

  preload (): void {
    this.load.glsl('bundle', 'assets/plasma-bundle.glsl')
    this.load.glsl('stars', 'assets/starfields.glsl')
  }

  create (): void {
    const shadersStarfields = [
      'Raymarched Plasma',
      'Marble',
      'Flower Plasma',
      'Plasma',
      'RGB Shift Field',
      'Layer Starfield',
      'Star Dots',
      'Retro Starfield',
      'Star Nest',
      'Star Tunnel',
      'Warp Speed',
      'Galaxy Trip'
    ]

    const MAX_HEIGHT = parseInt(`${conf.height ?? 0}`)
    const MAX_WIDTH = parseInt(`${conf.width ?? 0}`)
    let currentWidth = 0
    let currentHeight = 0
    let i = 0
    const draw = (sx, sy) => {
      return (current:string) => {
        currentHeight = Math.floor(i / 4) * Math.floor(MAX_HEIGHT / sy)
        currentWidth = (i % 4) * Math.floor(MAX_WIDTH / sx)
        this.add.shader(
          current,
          currentWidth,
          currentHeight,
          Math.floor(MAX_WIDTH / sx),
          Math.floor(MAX_HEIGHT / sy)
        ).setOrigin(0)
        i = i + 1
      }
    }
    shadersStarfields.forEach(draw(4, 3))
    //shadersPlasma.forEach(draw(4, 3))
    // this.add.rectangle(0, 0, MAX_HEIGHT, MAX_WIDTH, 0, 0.5)

    // this.input.on('wheel', ((me) => function (this: Scene, pointer) {
    //   console.log('changing from demo to tilemap', me)
    //   this.scene.start('tilemap')
    // })(called++), this)

    // this.tweens.add({
    //     targets: shad,
    //     duration: 1500,
    //     ease: 'Sine.inOut',
    //     yoyo: true,
    //     repeat: -1
    // })

    this.cameras.main.setBackgroundColor(0x0f0f0f)

    this.#bindCameraKeys()
  }

  update (time: number, delta: number): void {
    // const args = this.#iter.next().value;

    // this.cameras.main.centerOn(args.x, args.y)

    this.#controls.update(delta)
  }

  * getCameraPosition (): Generator<{ x: number, y: number }> {
    const endX = parseInt(`${conf.width ?? 0}`)
    const endY = parseInt(`${conf.height ?? 0}`)
    while (true) {
      for (let y = 0; y < endY; y += 10) {
        for (let x = 0; x < endX; x += 10) {
          yield { x, y }
        }
      }
    }
  }

  #bindCameraKeys (): void {
    if (this.input.keyboard === null) {
      return
    }

    this.#controls = new Cameras.Controls.SmoothedKeyControl({
      camera: this.cameras.main,
      left: this.input.keyboard.addKey(Input.Keyboard.KeyCodes.A),
      right: this.input.keyboard.addKey(Input.Keyboard.KeyCodes.D),
      up: this.input.keyboard.addKey(Input.Keyboard.KeyCodes.W),
      down: this.input.keyboard.addKey(Input.Keyboard.KeyCodes.S),
      zoomIn: this.input.keyboard.addKey(Input.Keyboard.KeyCodes.Q),
      zoomOut: this.input.keyboard.addKey(Input.Keyboard.KeyCodes.E),
      acceleration: 0.5,
      drag: 0.5,
      maxSpeed: 1
    })
  }
}
