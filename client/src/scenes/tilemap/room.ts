import { type Scene } from 'phaser'

export default {
  preload (scope: Scene): void {
    scope.load.image('tiles', 'assets/character and tileset/Dungeon_Tileset.png')
  },

  create (scope: Scene): void {
    const roomTileLeftOffset = 0
    const roomTileLeftMax = 6
    const roomTileTopOffset = 0
    const roomTileTopMax = 5
    const roomTiles = (() => {
      const result: number[] = []
      for (let i = roomTileTopOffset; i < roomTileTopMax; i++) {
        for (let j = roomTileLeftOffset; j < roomTileLeftMax; j++) {
          result.push(j + i * 10)
        }
      }
      return result
    })()
    const data = (() => {
      const result = new Array(roomTileTopMax)
      let z = 0
      for (let i = 0; i < roomTileTopMax; ++i) {
        result[i] = new Array(roomTileLeftMax)
        for (let j = 0; j < roomTileLeftMax; ++j) {
          result[i][j] = roomTiles[z++ % roomTiles.length]
        }
      }
      const pos = Math.floor(roomTileLeftMax / 2)
      result[roomTileTopMax - 1][pos - 1] = 36
      result[roomTileTopMax - 1][pos] = 37
      return result
    })()
    const tilemap = scope.make.tilemap({
      data,
      key: 'tilemap',
      tileHeight: 16,
      tileWidth: 16
    })
    tilemap.addTilesetImage('tiles')
    tilemap.createLayer('layer', 'tiles', 64, 64)
  },

  update (scope: Scene): void {
  }
}
