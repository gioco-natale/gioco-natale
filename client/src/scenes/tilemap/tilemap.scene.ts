import { Scene } from 'phaser'
import floor from './floor'
import room from './room'
const counters = {
  init: 0,
  preload: 0,
  create: 0
}

export default class TileMapScene extends Scene {
  constructor () {
    super({
      key: 'tilemap'
    })
  }

  init (): void {
    console.log(counters.init++)
  }

  preload (): void {
    console.log(counters.preload++)
    floor.preload(this)
    room.preload(this)
  }

  create (): void {
    console.log(counters.create++)
    floor.create(this)
    room.create(this)
    const width = parseInt(`${this.game.config.width ?? 0}`)
    const height = parseInt(`${this.game.config.height ?? 0}`)
    this.add.rectangle(0, 0, width, height, 0, 0.5)

    this.lights.addLight(16, 16, 128, 0xff0000, 1)
    this.lights.addLight(196, 64, 128, 0x00ff00, 1)
    this.lights.addLight(48, 196, 128, 0x0000ff, 1)

    // this.input.on('wheel', ((me) => function (this: Scene, pointer) {
    //   console.log('changing from tilemap to demo', me)
    //   this.scene.start('boot')
    // })(called++), this)
  }

  update (): void {
    floor.update(this)
    room.update(this)
  }
}
