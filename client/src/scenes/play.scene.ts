import { GameObjects, Math, Scene, Input, Physics, Types, Sound } from 'phaser'
import conf from '../conf'
import { Beam } from './play/beam'
import { Ghost } from './play/ghost'

export class PlayScene extends Scene {
  vampire: GameObjects.Sprite
  priest: GameObjects.Sprite
  skeleton: GameObjects.Sprite
  villains: Physics.Arcade.Group
  powerUps: Physics.Arcade.Group
  projectiles: GameObjects.Group
  player: Types.Physics.Arcade.SpriteWithDynamicBody
  cursorKeys: Types.Input.Keyboard.CursorKeys
  spacebar: Input.Keyboard.Key
  ratio:number = 420 / 256 * 2
  width:number = parseInt(`${conf.width ?? 0}`)
  height:number = parseInt(`${conf.height ?? 0}`)
  scoreLabel: GameObjects.Text
  scoreBox: GameObjects.Text
  score:number = 0
  beamSound: Sound.BaseSound
  explosonSound: Sound.BaseSound
  pickupSound: Sound.BaseSound
  music: Sound.BaseSound

  constructor(conf = {}) {
    super({
      ...conf,
      key: 'play'
    })
  }

  preload (): void {
    this.load.glsl('bundle', 'assets/plasma-bundle.glsl')
    this.load.glsl('stars', 'assets/starfields.glsl')
  }

  create(): void {
    this.initGroups()
    this.initBackground()
    this.createVillains()
    this.createPowerUps()
    this.createPlayer()
    this.createProjectiles()
    this.initInputs()
    this.initTexts()
    this.initAudio()
  }

  update(time: number, delta: number): void {
    this.handleMob('vampire', this.vampire, delta / 1500, delta / 20)
    this.handleMob('priest', this.priest, delta / 1000, delta / 30)
    this.handleMob('skeleton', this.skeleton, delta / 2500, delta / 40)
    //this.updateBackground(delta)
    this.getInput();
    if(Input.Keyboard.JustDown(this.spacebar) && this.player.active) {
      this.shoot()
    }
    this.projectiles.getChildren().forEach( current => {
      current.update();
    })
    this.scoreBox.text = this.getScore()
  }

  private initGroups() {
    this.villains = this.physics.add.group()
    this.powerUps = this.physics.add.group()
    this.projectiles = this.add.group()
  }

  private initBackground() {
    const shaders = [
      'Raymarched Plasma',
      'Marble',
      'Flower Plasma',
      'Plasma',
      'RGB Shift Field',
      'Layer Starfield',
      'Star Dots',
      'Retro Starfield',
      'Star Nest',
      'Star Tunnel',
      'Warp Speed',
      'Galaxy Trip'
    ]

    this.add.shader(
      shaders.at(
        globalThis.Math.floor(globalThis.Math.random() * shaders.length)
      ) ?? '',
      0,
      0,
      this.width,
      this.height
    ).setOrigin(0)

    this.add.rectangle(
      0,
      0,
      this.width,
      this.height,
      globalThis.Math.floor(globalThis.Math.random() * 0xffffff),
      .666
    )
    .setScale(this.ratio)
  }

  private createVillains() {
    this.createVillain('vampire', -1)
    this.createVillain('priest', 0)
    this.createVillain('skeleton', 1)
  }

  private createVillain(id:string, position) {
    this[id] = this.add.sprite(this.width / 2 + position * 96, this.height / 2 + 16, id)
    .setScale(this.ratio)
    this[id].play(`${id}_anim`)
    this[id].setInteractive()
    this[id].name = id
    this.villains.add(this[id])
  }

  private createPowerUps() {
    var maxObjects = 4
    for(var i = 0; i <= maxObjects; i++) {
      var powerUp = this.physics.add.sprite(0, 0, "coin_1") // ??
      this.powerUps.add(powerUp)
      powerUp.setRandomPosition(0, 0, this.width, this.height)
      powerUp.play(
        globalThis.Math.random() > 0.5 ? 'box_anim' : 'coin_anim'
      )
      powerUp.setVelocity(
        -100 + (globalThis.Math.random() * 200),
        -100 + (globalThis.Math.random() * 200),
      )
      powerUp.setCollideWorldBounds(true)
      powerUp.setBounce(1)
      powerUp.setScale(this.ratio)
    }
  }

  private createPlayer() {
    this.player = this.physics.add.sprite(
      this.width / 2 - 8,
      this.height - 64,
      "player"
    )

    this.player.play("player_move")
    this.player.setScale(this.ratio)
    this.player.setSize(16, 16)
    this.player.setCollideWorldBounds(true)
    this.physics.add.overlap(this.player, this.powerUps, this.pickPowerUp, undefined, this)
    this.physics.add.overlap(this.player, this.villains, this.hurtPlayer, undefined, this)
    this.physics.add.overlap(this.projectiles, this.villains, this.hitEnemy, undefined, this)
  }

  private createProjectiles() {
    this.physics.add.collider(this.projectiles, this.powerUps, function(projectile) {
      projectile.destroy()
    })
  }

  private initInputs() {
    // this.input.on("gameobjectdown", this.killMob)
    this.cursorKeys = this.input.keyboard?.createCursorKeys() ?? (()=>{throw new Error()})();
    this.spacebar = this.input.keyboard?.addKey(Input.Keyboard.KeyCodes.SPACE) ?? (()=>{throw new Error()})();
  }

  private initTexts() {
    const graphic = this.add.graphics()
    graphic.fillStyle(0xffffff, 0.9)
    graphic.moveTo(12, 35)
    graphic.lineTo(75, 35)
    graphic.lineTo(73, 38)
    graphic.lineTo(10, 38)
    graphic.closePath()
    graphic.fillPath()
    this.scoreLabel = this.add.text(10, 10, "Score: ", {
      fontFamily: "Crang",
      fontStyle: "italic"
    })
    this.scoreBox = this.add.text(90, 10, this.getScore(), {
      fontFamily: "alagard",
      fontSize: "3em"
    })
  }

  private initAudio() {
    this.beamSound = this.sound.add("beam_audio")
    this.explosonSound = this.sound.add("explosion_audio")
    this.pickupSound = this.sound.add("pickup_audio")

    this.music = this.sound.add("music", {
      mute: false,
      volume: 0.3,
      rate: 1,
      detune: 1,
      seek: 0,
      loop: true,
      delay: 0,
    })
    this.music.play()
  }

  private getScore() {
    const scoreMod = this.score < 0 ? - this.score : this.score
    return (this.score < 0 ? "-": "") + scoreMod.toString().padStart(10, "0")
  }

  private pickPowerUp(player:Types.Physics.Arcade.SpriteWithDynamicBody, powerUp:Types.Physics.Arcade.SpriteWithDynamicBody) {
    powerUp.disableBody(true, true)
    this.score += 0.2
    this.pickupSound.play();
  }

  private hurtPlayer(player:Types.Physics.Arcade.SpriteWithDynamicBody, enemy:Types.Physics.Arcade.SpriteWithDynamicBody) {
    this.resetMobPosition(enemy, enemy.name)
    if(player.alpha < 1) {
      return;
    }
    this.add.existing(new Ghost(this, player.x, player.y).setScale(this.ratio).setTint(0xff4242))
    player.disableBody(true, true)
    this.score -= 0.1
    this.time.addEvent({
      delay: 1000,
      callback: this.resetPlayerPosition,
      callbackScope: this,
      loop: false
    })
    this.explosonSound.play();
  }

  private hitEnemy(projectile:Types.Physics.Arcade.SpriteWithDynamicBody, enemy:Types.Physics.Arcade.SpriteWithDynamicBody) {
    this.add.existing(new Ghost(this, enemy.x, enemy.y).setScale(this.ratio).setAngle(180))
    projectile.destroy()
    this.resetMobPosition(enemy, enemy.name)
    this.score += 0.1
    this.explosonSound.play();
  }

  private shoot() {
    var bean = new Beam(this).setScale(this.ratio)
    this.add.existing(bean)
    this.projectiles.add(bean);
    this.beamSound.play();
  }

  private handleMob(id: string, mob, rot, speed): void {
    const itid = `id${id.substring(0, 1).toUpperCase() + id.substring(1)}`
    this[itid] = PlayScene.#rotate(mob, this[itid], rot)
    this[itid].next()
    this.moveMob(mob, speed, id)
  }

  private getInput(): void {
    if(this.cursorKeys.left.isDown) {
      this.player.setVelocityX(-200)
    }
    if(this.cursorKeys.right.isDown) {
      this.player.setVelocityX(200)
    }
    if(!this.cursorKeys.left.isDown && !this.cursorKeys.right.isDown) {
      this.player.setVelocityX(0)
    }
    if(this.cursorKeys.up.isDown) {
      this.player.setVelocityY(-200)
    }
    if(this.cursorKeys.down.isDown) {
      this.player.setVelocityY(200)
    }
    if(!this.cursorKeys.up.isDown && !this.cursorKeys.down.isDown) {
      this.player.setVelocityY(0)
    }
  }

  private moveMob(mob: GameObjects.Sprite, speed: number, id: string): void {
    mob.y += speed
    mob.x += globalThis.Math.random() * 8 - 4
    if (mob.y > this.height) {
      this.resetMobPosition(mob, id)
    }
  }

  private resetMobPosition(mob: GameObjects.Sprite, id: string) {
    Object.assign(mob, { y: 0, x: Math.Between(0, this.width) })
    mob.setTexture(id)
    mob.play(`${id}_anim`)
  }

  private resetPlayerPosition() {
    this.player.alpha = 0.5
    this.player.enableBody(true, this.width / 2 - 8, this.height, true, true)
    this.tweens.add({
      targets: this.player,
      y: this.height - 64,
      ease: 'Power1',
      duration: 500,
      repeat: 0,
      onComplete: () => this.player.alpha = 1
    })
  }

  // private killMob(_pointer, mob: GameObjects.Sprite) {
  //   mob.setTexture("ghost")
  //   mob.play("ghost_anim")
  //   mob.stopAfterRepeat(1)
  // }

  itPriest: Generator<number>
  itSkeleton: Generator<number>
  itVampire: Generator<number>

  static #rotate(image: GameObjects.Image, it: Generator<number>, rotation: number): Generator<number> {
    const next = it?.next() ?? { done: true }
    if (next.done ?? false) {
      it = rotate(image, rotation)
      return this.#rotate(image, it, rotation)
    }
    return it

    function* rotate(image: GameObjects.Image, rotation: number): Generator<number> {
      while (!(image.rotation < 0 && image.rotation + rotation > 0)) {
        yield image.rotation += rotation
      }
      image.rotation = 0
    }
  }
}
