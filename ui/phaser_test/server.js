const http = require('http');
const fs = require('fs');
const path = require('path');

const port = 8080;

const requestHandler = (request, response) => {
  console.log(`Received request for ${request.url}`);
  if (request.url.startsWith('/assets/')) {
    const filePath = path.join(__dirname, request.url);
    const headers = {
      'Access-Control-Allow-Origin': '*'
    };
    fs.readFile(filePath, (error, content) => {
      if (error) {
        response.writeHead(404, headers);
        response.end(`File not found: ${request.url}`);
      } else {
        response.writeHead(200, headers);
        response.end(content);
      }
    });
  } else {
    response.writeHead(404);
    response.end('Not found');
  }
};

const server = http.createServer(requestHandler);

server.listen(port, (error) => {
  if (error) {
    console.log(`Failed to start server: ${error}`);
  } else {
    console.log(`Server listening on port ${port}`);
  }
});
