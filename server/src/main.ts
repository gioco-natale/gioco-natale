import * as utils from './utils';

const exitSignals = ['SIGINT', 'SIGTERM', 'SIGQUIT'];

export default void (async function main(utils) {
  const server = await utils.createServer(
    utils.getUserSettings(
      process.env.GIOCO_NATALE_SERVER_OVERRIDE_SETTINGS ?? '/dev/null',
    ),
  );
  utils.bindExitEvents(exitSignals, server);
  utils.startServer(server);
  return server;
})(utils);
