import { GameServer } from './server';
import * as defaultSettings from './settings';
import { readFile } from 'fs/promises';

export async function createServer(args) {
  process.stdout.write('Starting...\r');
  const settings = {
    ...defaultSettings,
    ...(await args),
  };
  return await new GameServer(settings);
}

export function bindExitEvents(exitSignals, server: GameServer) {
  server.addEventListener('stop', () => {
    server.settings.DEBUG_LOG('\rExiting.');
    process.exit();
  });
  return exitSignals.forEach((signal) =>
    process.on(signal, stopServer.bind(this, server)),
  );
}

export async function startServer(server: GameServer) {
  try {
    return await server.start();
  } catch (e) {
    server.settings.DEBUG_LOG('Dying! Reason:', e);
  } finally {
    server.settings.DEBUG_LOG('Main done.');
  }
}

export async function stopServer(server: GameServer) {
  server.settings.DEBUG_LOG('Stopping server.');
  return await server.stop();
}

export async function getUserSettings(path) {
  if (!path) {
    return undefined;
  }
  try {
    const buffer = await readFile(path);
    const file = buffer.toString();
    return JSON.parse(file);
  } catch (e) {
    return undefined;
  }
}
