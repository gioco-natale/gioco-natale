import { Module } from '@nestjs/common';
import { AppModule } from './app.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { RouterModule } from '@nestjs/core';
import { InfoModule } from './info/info.module';
import { ListenModule } from './game/listen/listen.module';
import { CreateModule } from './game/create/create.module';
import { JoinModule } from './game/join/join.module';
import { GameModule } from './game/game.module';

@Module({
  imports: [
    AppModule,
    ServeStaticModule.forRoot({
      rootPath: join(
        process.env.GIOCO_NATALE_RELATIVE_ABSOLUTE_PATH ||
          process.cwd() +
            '/' +
            (process.env.GIOCO_NATALE_RELATIVE_PUBLIC_PATH || '../public'),
      ),
      renderPath: '/',
    }),
    RouterModule.register([
      {
        path: '/',
        module: InfoModule
      },
      {
        path: '/',
        children: [
          {
            path: '/v1',
            children: [
              {
                path: '/game',
                children: [ListenModule, CreateModule, JoinModule],
              },
            ],
          },
        ],
      },
    ]),
  ],
})
export class WebModule {}
