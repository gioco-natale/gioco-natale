import { Controller, MessageEvent, Param, Sse } from '@nestjs/common';
import { ListenService } from './listen.service';
import { Observable } from 'rxjs';
import { ApiParam, ApiProduces, ApiTags } from '@nestjs/swagger';

@ApiTags('Game')
@Controller(':id/listen')
export class ListenController {
  constructor(private readonly service: ListenService) {}

  @Sse()
  @ApiParam({
    name: 'id',
    description: 'The id of the game',
  })
  @ApiProduces('text/event-stream')
  listen(@Param('id') id: string): Observable<MessageEvent> {
    return this.service.getUpdates(id);
  }
}
