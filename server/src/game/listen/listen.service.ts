import { Injectable, MessageEvent } from '@nestjs/common';
import { Observable, fromEvent, map } from 'rxjs';
import db from '../game.db';

@Injectable()
export class ListenService {
  getUpdates(id: string): Observable<MessageEvent> {
    return fromEvent(db, id).pipe(map(({ data }: any) => ({ data })));
  }
}
