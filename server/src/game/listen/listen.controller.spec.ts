import { Test, TestingModule } from '@nestjs/testing';
import { ListenController } from './listen.controller';
import { ListenService } from './listen.service';
import db from '../game.db';

describe('ListenController', () => {
  let controller: ListenController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ListenController],
      providers: [ListenService],
    }).compile();

    controller = app.get<ListenController>(ListenController);
  });

  describe('root', () => {
    it('should return listen the game updates', async () => {
      let i = 2;
      await mockCreateGame();
      const sub = controller.listen('xx').subscribe((response) => {
        if (!--i) {
          sub.unsubscribe();
        }
      });
      await mockJoinGame();
      await mockJoinGame();
      expect(i).toBe(0);
    });
  });
});

function mockCreateGame() {
  db.value('xx', { players: 0 });
}

function mockJoinGame() {
  db.value('xx.players', db.value('xx.players') + 1);
}
