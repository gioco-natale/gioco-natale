import { Module } from '@nestjs/common';
import { CreateModule } from './create/create.module';
import { ListenModule } from './listen/listen.module';
import { JoinModule } from './join/join.module';

@Module({
  imports: [CreateModule, ListenModule, JoinModule]
})
export class GameModule {}
