import { DEBUG_LOG } from '../settings';

export class Db extends EventTarget {
  #db = {};
  value(pathRow: string, value?: any) {
    const path = pathRow.split(/\.|\[(.*?)\]/g).filter((x) => x);
    DEBUG_LOG(path);
    return path.reduce((prev, curr) => {
      DEBUG_LOG(prev, curr);
      if (typeof prev[curr] === 'object') {
        return prev[curr];
      }
      if (arguments.length > 1) {
        prev[curr] = value;
        this.#triggerEvent(path, prev);
        return prev[curr];
      }
      return prev[curr];
    }, this.#db);
  }

  #triggerEvent(path, data) {
    const event = new (class extends Event {
      data;
      path;
    })(path[0]);
    event.data = data;
    event.path = path;
    this.dispatchEvent(event);
  }
}

export default new Db();
