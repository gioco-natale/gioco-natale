import { Injectable } from '@nestjs/common';
import db from '../game.db';
import { randomUUID } from 'crypto';

@Injectable()
export class CreateService {
  createGame(game = {}) {
    const id = randomUUID();
    db.value(id, {
      ...game,
      id,
      players: 0,
    });

    return db.value(id);
  }
}
