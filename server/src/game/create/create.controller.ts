import { Body, Controller, Post } from '@nestjs/common';
import { CreateService } from './create.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Game')
@Controller('create')
export class CreateController {
  constructor(private readonly createService: CreateService) {}

  @Post()
  createGame(@Body() game) {
    return this.createService.createGame(game);
  }
}
