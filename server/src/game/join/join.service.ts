import { Injectable } from '@nestjs/common';
import db from '../game.db';

@Injectable()
export class JoinService {
  joinGame(id: string) {
    db.value(`${id}.players`, db.value(`${id}.players`) + 1);
  }
}
