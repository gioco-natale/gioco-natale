import { Controller, Get, Param } from '@nestjs/common';
import { JoinService } from './join.service';
import { ApiParam, ApiTags } from '@nestjs/swagger';

@ApiTags('Game')
@Controller(':id/join')
export class JoinController {
  constructor(private readonly joinService: JoinService) {}
  @Get()
  @ApiParam({
    name: 'id',
    description: 'The id of the game',
  })
  joinGame(@Param('id') id: string) {
    return this.joinService.joinGame(id);
  }
}
