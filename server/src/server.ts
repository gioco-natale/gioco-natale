import { getPackageJsonConf } from './settings';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { WebModule } from './web.module';

export class GameServer extends EventTarget {
  #app = NestFactory.create(WebModule, {
    httpsOptions: this.settings['https'],
    ...this.settings['web'],
  });

  get app() {
    return this.#app;
  }

  #running = false;

  get running() {
    return this.#running;
  }

  get settings() {
    return this._settings;
  }

  constructor(private _settings: any = {}) {
    if (!_settings['https']) {
      console.warn(
        'Warning! Falling back to HTTP only. Connections will not be safe.',
      );
    }
    super();
  }

  async start() {
    const app = await this.#setup();
    return app
      .listen(parseInt(this.settings['http']['port'], 10))
      .then((server) => {
        this.#running = true;
        this.dispatchEvent(
          new GameServerCustomEvent('start', { detail: { server, app } }),
        );
        return server;
      });
  }

  async stop() {
    const app = await this.app;
    app.close().finally(() => {
      this.#running = false;
      this.dispatchEvent(
        new GameServerCustomEvent('stop', { detail: { app } }),
      );
    });
  }

  async #setup() {
    this.#setupPrefix();
    this.#setupSwagger();
    return await this.app;
  }

  async #setupPrefix() {
    const app = await this.app;
    app.setGlobalPrefix('api');
  }

  async #setupSwagger() {
    const app = await this.app;
    return SwaggerModule.setup(
      'api/docs',
      app,
      SwaggerModule.createDocument(
        app,
        new DocumentBuilder()
          .setTitle('Gioco Natale')
          .setDescription('API for Gioco Natale')
          .setVersion(getPackageJsonConf().version)
          .addTag('Game', 'Gioco Natale API')
          .addTag('Extra')
          .build(),
      ),
    );
  }
}

class GameServerCustomEvent extends Event {
  // Polyfills CustomEvent in node
  #details;
  get details() {
    return this.#details;
  }
  constructor(name, { detail = undefined, ...dict } = {}) {
    super(name, dict);
    this.#details = detail;
  }
}
