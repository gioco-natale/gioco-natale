import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GameModule } from './game/game.module';
import { InfoModule } from './info/info.module';

@Module({
  imports: [ConfigModule.forRoot(), InfoModule, GameModule],
})
export class AppModule {}
