import { readFileSync } from 'fs';
import { resolve } from 'path';

export const DEBUG = process.env.GIOCO_NATALE_SERVER_DEBUG ?? false;

export const http = {
  port: process.env.GIOCO_NATALE_SERVER_PORT ?? '3000',
};

export const https = (function (key, cert) {
  try {
    if (!key || !cert) {
      throw `key: ${key}; cert: ${cert}`;
    }
    return {
      key: readFileSync(key),
      cert: readFileSync(cert),
    };
  } catch (e) {
    DEBUG_LOG('Default https settings initialization failed.', e);
  }
  return;
})(
  process.env.GIOCO_NATALE_SERVER_PRIVATE_KEY,
  process.env.GIOCO_NATALE_SERVER_PUBLIC_CERTIFICATE_PEM,
);

export function DEBUG_LOG(msg, ...data) {
  const dbgMsg = 'Debug info:';
  if (!(this ? this.DEBUG : DEBUG)) {
    return;
  }
  if (!data.length) {
    return console.debug(dbgMsg, msg);
  }
  console.groupCollapsed(dbgMsg);
  console.debug(msg);
  data.forEach(console.dir.bind(console));
  console.groupEnd();
}

export function getPackageJsonConf() {
  return JSON.parse(
    readFileSync(resolve(__dirname, '../package.json')).toString(),
  );
}

export const getStaticPackageJsonConf = (function () {
  const pkg = getPackageJsonConf();
  return () => pkg;
})();

export function getCmdArgs(argv, ...inputs) {
  const initial: any = inputs.reduce((prev, curr) => {
    prev[curr.field] = curr.value;
    return prev;
  }, {});

  return argv.reduce((prev, curr) => {
    let regexp = null;
    const selected = inputs.find((input) =>
      input.regexp.find((x) => {
        const exists = x.test(curr);
        regexp = x;
        return exists;
      }),
    );
    return (selected && selected.handler(prev, curr, regexp)) || prev;
  }, initial);
}

export const COMMON_FLAGS = [
  {
    field: 'filename',
    regexp: [/^--port=(\d+)/, /^-p(\d+)/],
    handler(prev, curr, regexp) {
      const port = curr.replace(regexp, '$1');
      process.env['GIOCO_NATALE_SERVER_PORT'] = `${port}`;
      http.port = process.env['GIOCO_NATALE_SERVER_PORT'];
      this.value = prev[
        this.field
      ] = `./.${process.env['GIOCO_NATALE_SERVER_PORT']}.pid`;
    },
    value: './.pid',
  },
  {
    field: 'force',
    regexp: [/^--force/],
    handler: (prev) => {
      prev.force = true;
    },
    value: false,
  },
];
