import { Controller, Get } from '@nestjs/common';
import { InfoService } from './info.service';
import { ApiTags } from '@nestjs/swagger';
import { InfoDto } from './info.dto';

@ApiTags('Extra')
@Controller('info')
export class InfoController {
  constructor(private readonly infoService: InfoService) {}

  @Get()
  getInfo(): InfoDto {
    return this.infoService.getInfo();
  }
}
