import { Test, TestingModule } from '@nestjs/testing';
import { InfoController } from './info.controller';
import { InfoService } from './info.service';
import { execSync } from 'child_process';

describe('InfoController', () => {
  let controller: InfoController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [InfoController],
      providers: [InfoService],
    }).compile();

    controller = app.get<InfoController>(InfoController);
  });

  describe('root', () => {
    it('should return the info of the server', () => {
      expect(controller.getInfo().version).toBe(
        execSync('npm pkg get version')
          .toString()
          .replace(/.*([\d]+\.[\d]+\.[\d]+).*$/s, '$1'),
      );
    });
  });
});
