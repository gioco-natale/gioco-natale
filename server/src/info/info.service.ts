import { Injectable } from '@nestjs/common';
import { InfoDto } from './info.dto';
import { getPackageJsonConf } from '../settings';

@Injectable()
export class InfoService {
  getInfo(): InfoDto {
    const pkg = getPackageJsonConf();
    return {
      name: pkg.name,
      version: pkg.version,
      checksum: pkg.checksum,
      description: pkg.description,
      revision: pkg.revision
    };
  }
}
