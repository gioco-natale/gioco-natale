import { ApiProperty } from '@nestjs/swagger';
import { getStaticPackageJsonConf } from '../settings';

export class InfoDto {
  @ApiProperty({
    example: getStaticPackageJsonConf().name,
    description: 'The package name',
  })
  name: string;

  @ApiProperty({
    example: getStaticPackageJsonConf().version,
    description: 'The package version',
  })
  version: string;

  @ApiProperty({
    example: getStaticPackageJsonConf().checksum,
    description: 'A md5 checksum of the package files',
  })
  checksum: string;

  @ApiProperty({
    example: getStaticPackageJsonConf().checksum,
    description: 'The revision hash',
  })
  revision: string;

  @ApiProperty({
    example: getStaticPackageJsonConf().description,
    description: 'The package description',
  })
  description: string;

  [x: string]: any;
}
