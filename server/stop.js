#!/usr/bin/env node

const { readFileSync, rmSync } = require("fs")
const settings = require("./dist/settings.js")

const flags = settings.getCmdArgs(process.argv, ...settings.COMMON_FLAGS)

try {
    process.kill(readFileSync(flags.filename))
} catch (e) {
    if(!flags.force) {
        throw e
    }
    console.warn(e)
    console.warn("--force is present skipping")
}

try {
    rmSync(flags.filename)
} catch (e) {
    if(!flags.force) {
        throw e
    }
    console.warn(e)
    console.warn("--force is present: skipping")
}
