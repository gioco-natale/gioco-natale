#!/usr/bin/env node
const settings = require("./dist/settings.js")

const { writeFileSync, rmSync } = require("fs")

process.stdout.write("The Veloperz!\r")

const flags = settings.getCmdArgs(process.argv, ...settings.COMMON_FLAGS)

if(flags.force) {
    require("./stop")
}

writeFileSync(flags.filename, `${process.pid}`);
['SIGINT', 'SIGTERM', 'SIGQUIT'].forEach(curr => process.on(curr, () => rmSync(flags.filename)))

require("./dist/main")
